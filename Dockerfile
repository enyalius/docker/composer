FROM alpine:3.16
MAINTAINER Marcio Bigolin <marcio.bigolinn@gmail.com>
LABEL Description="Imagem para usar o composer para a Locaweb"

RUN apk --update add curl \
          php8 php8-sodium php8-phar php8-iconv \
          php8-openssl php8-curl php8-ctype php8-mbstring \
          php8-gd php8-tokenizer php8-dom php8-gmp \
          php8-json php8-simplexml php8-xml php8-xmlwriter\
          && rm /var/cache/apk/*

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer 
WORKDIR /app

